import Vue from 'vue'
import contact from '../components/contact.vue'
import VueGeolocationApi from 'vue-geolocation-api'

Vue.component('contact', contact)
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
Vue.use(VueGeolocationApi)
